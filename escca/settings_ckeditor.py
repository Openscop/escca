# CKEditor configuration options
# https://github.com/django-ckeditor/django-ckeditor

CKEDITOR_IMAGE_BACKEND = 'pillow'
CKEDITOR_JQUERY_URL = '//code.jquery.com/jquery-1.11.1.min.js'
CKEDITOR_UPLOAD_PATH = "uploads/"
CKEDITOR_CONFIGS = {
    'default': {
        'toolbar_Full': [
            ['Format'],
            ['PasteText', 'PasteFromWord'],
            ['Undo', 'Redo'],
            ['Bold', 'Italic', 'Underline', 'Strike', 'Blockquote', '-', 'Subscript', 'Superscript'],
            ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent'],
            ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
            ['Table', '-', 'Image', 'Link', 'Unlink'],
            ['HorizontalRule', 'SpecialChar'],
            ['Maximize', 'ShowBlocks'],
        ],
        'toolbar': 'Full',
        'width': '100%',
        'skin': 'bootstrapck',
    },
}
CKEDITOR_RESTRICT_BY_USER = True

# http://django-endless-pagination.readthedocs.org/en/latest/customization.html

ENDLESS_PAGINATION_PER_PAGE = 15
ENDLESS_PAGINATION_PREVIOUS_LABEL = "&laquo;"
ENDLESS_PAGINATION_NEXT_LABEL = "&raquo;"

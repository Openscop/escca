from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.utils.translation import ugettext_lazy as _
from django.views.decorators.cache import never_cache

from django.contrib import admin
from django.contrib.auth.decorators import login_required

from quiz.views import PPPView
from quiz.views import ProfileView
from quiz.views import ProfileUpdateView
from quiz.views import TopicResultsView
from quiz.views import UserTopicsView
from quiz.views import ContactView

from quiz.views import IndexView

from ckeditor import views

admin.autodiscover()
admin.site.site_header = _('ESCCA administration')
admin.site.site_title = _('ESCCA site admin')

urlpatterns = [
    # Examples:
    # url(r'^$', 'escca.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^accounts/profile/$', ProfileView.as_view(), name='profile'),
    url(r'^accounts/profile/topic/(?P<pk>\d+)$', TopicResultsView.as_view(), name='topic_results'),
    url(r'^accounts/profile/edit$', ProfileUpdateView.as_view(), name='profile_edit'),
    url(r'^accounts/profile/topics/edit$', UserTopicsView.as_view(), name='topics_edit'),
    url(r'^accounts/profile/ppp$', PPPView.as_view(), name='ppp'),
    url(r'^contact$', ContactView.as_view(), name='contact'),
    # We must override those urls in order to make them login_required instead of staff_required
    #url(r'^ckeditor/', include('ckeditor.urls')),
    url(r'^ckeditor/upload/', login_required(views.upload), name='ckeditor_upload'),
    url(r'^ckeditor/browse/', never_cache(login_required(views.browse)), name='ckeditor_browse'),
    url(r'^quiz/', include('quiz.urls', namespace='quiz')),
    url(r'^cytometry/', include('cytometry.urls', namespace='cytometry')),
    url(r'^directory/', include('directory.urls', namespace='directory')),
    url(r'^sponsors/', include('sponsors.urls', namespace='sponsors')),
    url(r'^lectures/', include('lectures.urls', namespace='lectures')),
    url(r'^tutorships/', include('tutorships.urls', namespace='tutorships')),
    url(r'^$', IndexView.as_view(), name='index'),

    url(r'^pages/', include('django.contrib.flatpages.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

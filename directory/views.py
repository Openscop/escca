from __future__ import unicode_literals

from string import ascii_uppercase

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.utils.decorators import method_decorator

from django.views.generic.base import TemplateView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView


class IndexView(TemplateView):
    template_name = 'directory/index.html'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(IndexView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['alphabet'] = ascii_uppercase
        return context


class UserDetailView(DetailView):
    model = User
    template_name = 'directory/user_detail.html'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(UserDetailView, self).dispatch(*args, **kwargs)


class UserListView(ListView):
    model = User
    template_name = 'directory/user_list.html'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(UserListView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(UserListView, self).get_context_data(**kwargs)
        context['alphabet'] = ascii_uppercase
        context['current_search'] = self.kwargs['letter']
        return context

    def get_queryset(self, **kwargs):
        return User.objects.filter(last_name__istartswith=self.kwargs['letter'])

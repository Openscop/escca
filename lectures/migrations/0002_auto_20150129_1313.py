# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lectures', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='lecture',
            name='quizzes',
            field=models.ManyToManyField(to='quiz.Quiz', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='lecture',
            name='teachers',
            field=models.ManyToManyField(to='quiz.Profile', null=True, verbose_name='teachers', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='session',
            name='attendees',
            field=models.ManyToManyField(to='quiz.Profile', null=True, verbose_name='attendees', blank=True),
            preserve_default=True,
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0031_auto_20150330_1341'),
        ('lectures', '0006_auto_20150211_1412'),
    ]

    operations = [
        migrations.CreateModel(
            name='LectureSitting',
            fields=[
                ('sitting_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True,
                                                     serialize=False, to='quiz.Sitting')),
                ('first_attempt', models.BooleanField(default=False)),
                ('session', models.ForeignKey(verbose_name='session', to='lectures.Session')),
            ],
            options={
                'abstract': False,
            },
            bases=('quiz.sitting',),
        ),
    ]

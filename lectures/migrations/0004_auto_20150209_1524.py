# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('lectures', '0003_auto_20150129_1440'),
    ]

    operations = [
        migrations.AlterField(
            model_name='lecture',
            name='description',
            field=ckeditor.fields.RichTextField(verbose_name='description'),
            preserve_default=True,
        ),
    ]

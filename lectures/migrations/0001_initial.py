# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0028_question_ponderation'),
    ]

    operations = [
        migrations.CreateModel(
            name='Lecture',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='title')),
                ('description', models.TextField(verbose_name='description')),
                ('quizzes', models.ManyToManyField(to='quiz.Quiz')),
                ('teachers', models.ManyToManyField(to='quiz.Profile', verbose_name='teachers')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Session',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('start', models.DateTimeField(verbose_name='start of the session')),
                ('end', models.DateTimeField(verbose_name='end the session')),
                ('place', models.TextField(verbose_name='place')),
                ('attendees', models.ManyToManyField(to='quiz.Profile', verbose_name='attendees')),
                ('lecture', models.ForeignKey(verbose_name='lecture', to='lectures.Lecture')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]

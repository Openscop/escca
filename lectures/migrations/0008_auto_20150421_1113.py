# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('lectures', '0007_lecturesitting'),
    ]

    operations = [
        migrations.AlterField(
            model_name='lecture',
            name='quizzes',
            field=models.ManyToManyField(to='quiz.Quiz', blank=True),
        ),
        migrations.AlterField(
            model_name='lecture',
            name='teachers',
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL, verbose_name='teachers', blank=True),
        ),
        migrations.AlterField(
            model_name='session',
            name='attendees',
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL, verbose_name='attendees', blank=True),
        ),
    ]

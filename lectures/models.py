from __future__ import unicode_literals

import datetime

from django.db import models
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.db.models.signals import m2m_changed
from django.dispatch import receiver
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _

from ckeditor.fields import RichTextField

from guardian.shortcuts import assign_perm, remove_perm

from cytometry.models import Topic
from quiz.models import LEVEL_CHOICES
from quiz.models import Quiz
from quiz.models import Sitting
from quiz.models import SittingQuerySet


@python_2_unicode_compatible
class Lecture(models.Model):
    title = models.CharField(_('title'), max_length=255)
    description = RichTextField(_('description'))
    teachers = models.ManyToManyField(User, verbose_name=_('teachers'), blank=True)
    quizzes = models.ManyToManyField(Quiz, blank=True)
    topic = models.ForeignKey(Topic, verbose_name=_('topic'))
    level = models.CharField(_('level'), max_length=1,
                             choices=LEVEL_CHOICES)
    lectures = models.IntegerField(_('lecture hours'))
    data_analysis = models.IntegerField(_('data analysis'))
    bench_practice = models.IntegerField(_('bench practice'))

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('lectures:lecture-detail', kwargs={'pk': self.pk})

    def get_teacher_points(self):
        return sum([session.get_teacher_points() for session in self.session_set.all()])


@receiver(m2m_changed, sender=Lecture.teachers.through)
def update_quiz_review_perms(action, instance, pk_set, **kwargs):
    if action == "post_add":
        for pk in pk_set:
            user = User.objects.get(pk=pk)
            if not user.has_perm('change_lecture', instance):
                assign_perm('change_lecture', user, instance)
            if not user.has_perm('delete_lecture', instance):
                assign_perm('delete_lecture', user, instance)
    elif action == "post_remove":
        for pk in pk_set:
            user = User.objects.get(pk=pk)
            if user.has_perm('change_lecture', instance):
                remove_perm('change_lecture', user, instance)
            if user.has_perm('delete_lecture', instance):
                remove_perm('delete_lecture', user, instance)


@python_2_unicode_compatible
class Session(models.Model):
    lecture = models.ForeignKey(Lecture, verbose_name=_('lecture'))
    start = models.DateTimeField(_('start of the session'))
    end = models.DateTimeField(_('end of session'))
    place = models.TextField(_('location'))
    language = models.CharField(_('language'), max_length=255)
    registration = models.URLField(_('registration link'), blank=True)
    attendees = models.ManyToManyField(User, verbose_name=_('attendees'), blank=True)

    def __str__(self):
        return self.lecture.title

    def get_absolute_url(self):
        return reverse('lectures:lecture-detail',
                       kwargs={'pk': self.lecture.pk})

    def post_quiz_begin(self):
        return self.end + datetime.timedelta(weeks=2)

    def post_quiz_end(self):
        return self.end + datetime.timedelta(weeks=6)

    def get_initial_attendance(self):
        sittings = self.lecturesitting_set.introductory_attempt().count()
        potential_sittings = self.lecture.quizzes.count() * self.attendees.count()
        try:
            return sittings * 100 / potential_sittings
        except ZeroDivisionError:
            return None

    def get_final_attendance(self):
        sittings = self.lecturesitting_set.final_attempt().count()
        potential_sittings = self.lecture.quizzes.count() * self.attendees.count()
        try:
            return sittings * 100 / potential_sittings
        except ZeroDivisionError:
            return None

    def get_teacher_points(self):
        return sum([quiz.get_max_score() for quiz in self.lecture.quizzes.all()]) * self.attendees.count()


class LectureSittingQuerySet(SittingQuerySet):
    def final_attempt(self):
        return self.exclude(first_attempt=True)

    def introductory_attempt(self):
        return self.filter(first_attempt=True)


@python_2_unicode_compatible
class LectureSitting(Sitting):
    first_attempt = models.BooleanField(default=False)
    session = models.ForeignKey(Session, verbose_name=_('session'))
    objects = LectureSittingQuerySet.as_manager()

    def __str__(self):
        return "{}".format(self.sitting_ptr)

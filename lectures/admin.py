from __future__ import unicode_literals

from django.contrib import admin

from guardian.admin import GuardedModelAdmin

from .models import Lecture
from .models import Session


class SessionInline(admin.TabularInline):
    model = Session
    extra = 1


class LectureAdmin(GuardedModelAdmin):
    inlines = [
        SessionInline,
    ]
    search_fields = [
        'title',
        'teachers__user__first_name',
        'teachers__user__last_name',
        'teachers__user__username',
        'quizzes__title',
    ]


admin.site.register(Lecture, LectureAdmin)

from __future__ import unicode_literals

from itertools import groupby
from operator import attrgetter

from django import forms
from django.conf import settings
from django.core.mail import EmailMessage
from django.forms.fields import ChoiceField
from django.utils.translation import ugettext_lazy as _

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Fieldset, Div, Field

from extra_views import InlineFormSet

from cytometry.models import Topic

from .models import Answer
from .models import Profile
from .models import ProfileTopicsOfInterest
from .models import Quiz
from .models import Question


class EsccaFormHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super(EsccaFormHelper, self).__init__(*args, **kwargs)
        self.form_class = 'form-horizontal'
        self.label_class = 'col-sm-2'
        self.field_class = 'col-sm-10'
        self.form_tag = False


class ContactForm(forms.Form):
    email = forms.EmailField(label=_('Your email address'))
    subject = forms.CharField()
    message = forms.CharField(widget=forms.Textarea)

    def __init__(self, *args, **kwargs):
        super(ContactForm, self).__init__(*args, **kwargs)
        self.helper = EsccaFormHelper()
        self.helper.form_tag = True
        self.helper.add_input(Submit('submit', _('Send')))

    def send_email(self, email, subject, message):
        email = EmailMessage(
            subject=subject,
            body=message,
            to=[settings.EMAIL_HOST_USER],
            headers={'Reply-To': email}
        )
        email.send()


class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        exclude = ('user', 'topics_of_interest', 'fees_payed')
        widgets = {
            'lab_name': forms.TextInput(attrs={'placeholder': _('Lab name')}),
            'city': forms.TextInput(attrs={'placeholder': _('City')}),
            'lab_name_url': forms.URLInput(attrs={'placeholder': _('http://www.example.com')}),
        }

    def __init__(self, *args, **kwargs):
        super(ProfileForm, self).__init__(*args, **kwargs)
        self.helper = EsccaFormHelper()
        self.helper.form_tag = True
        self.helper.layout = Layout(
            Fieldset(
                _('General informations'),
                'lab_name',
                'lab_name_url',
                'city',
                'country',
            ),
            Fieldset(
                _('Would you like to be notified of new quizzes ?'),
                'accept_emails',
            ),
        )
        self.helper.add_input(Submit('submit', 'Submit'))


class UserTopicsForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ()

    def __init__(self, *args, **kwargs):
        super(UserTopicsForm, self).__init__(*args, **kwargs)
        self.helper = EsccaFormHelper()


class TopicChoiceField(forms.ModelChoiceField):
    def _get_choices(self):
        if hasattr(self, '_choices'):
            return self._choices
        iterator = groupby(self.queryset, attrgetter('subfield'))
        self.choices = ((
            " - ".join([subfield.field.__str__(), subfield.__str__()]),
            [(topic.id, self.label_from_instance(topic))for topic in topics]
        ) for subfield, topics in iterator)
        self.choices = [('', self.empty_label)] + self.choices
        return self.choices

    choices = property(_get_choices, ChoiceField._set_choices)


class TopicForm(forms.ModelForm):
    topic = TopicChoiceField(queryset=Topic.objects.all().order_by('subfield__field', 'subfield', 'name'),
                             required=False,
                             empty_label="---------")

    class Meta:
        model = Profile.topics_of_interest.through
        exclude = ('profile', )

    def __init__(self, *args, **kwargs):
        super(TopicForm, self).__init__(*args, **kwargs)
        self.helper = EsccaFormHelper()
        self.helper.layout = Layout(
            Div('topic', css_class='col-sm-6'),
            Div('is_reviewer', Field('id', type="hidden"), css_class='col-sm-3'),
            Div('DELETE', css_class='col-sm-3'),
        )


class TopicInline(InlineFormSet):
    model = ProfileTopicsOfInterest
    form_class = TopicForm
    extra = 1


class SignupForm(ProfileForm):
    first_name = forms.CharField(
        max_length=30,
        widget=forms.TextInput(attrs={'placeholder': _('First name')})
    )
    last_name = forms.CharField(
        max_length=30,
        widget=forms.TextInput(attrs={'placeholder': _('Last name')})
    )

    def __init__(self, *args, **kwargs):
        super(SignupForm, self).__init__(*args, **kwargs)
        self.helper = EsccaFormHelper()
        self.helper.form_tag = True
        self.helper.layout = Layout(
            Fieldset(
                _('Account information'),
                'first_name',
                'last_name',
                'username',
                'email',
                'password1',
                'password2',
            ),
            Fieldset(
                _('General informations'),
                'lab_name',
                'lab_name_url',
                'city',
                'country',
            ),
            Fieldset(
                _('Would you like to be notified of new quizzes ?'),
                'accept_emails',
            ),
        )
        self.helper.add_input(Submit('submit', 'Sign Up'))

    def signup(self, request, user):
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.save()
        Profile.objects.create(
            user=user,
            country=self.cleaned_data['country'],
            city=self.cleaned_data['city'],
            lab_name=self.cleaned_data['lab_name'],
            lab_name_url=self.cleaned_data['lab_name_url'],
            accept_emails=self.cleaned_data['accept_emails'],
        )


class QuizForm(forms.ModelForm):
    topic = TopicChoiceField(queryset=Topic.objects.all().order_by('subfield__field', 'subfield', 'name'),
                             required=False,
                             empty_label="---------")

    class Meta:
        model = Quiz
        exclude = ['authors', 'state', 'pub_date', 'reviewers']
        widgets = {
            'get_home_message': forms.Textarea(attrs={'rows': 1}),
        }

    def __init__(self, *args, **kwargs):
        super(QuizForm, self).__init__(*args, **kwargs)
        self.helper = EsccaFormHelper()


class QuizCommentForm(forms.Form):
    message = forms.CharField(widget=forms.Textarea)

    def __init__(self, *args, **kwargs):
        super(QuizCommentForm, self).__init__(*args, **kwargs)
        self.helper = EsccaFormHelper()
        self.helper.form_tag = True
        self.helper.add_input(Submit('submit', _('Send')))


class QuestionForm(forms.ModelForm):
    class Meta:
        model = Question
        fields = ('question', 'explanation', 'ponderation', 'correct', )
        widgets = {
            'question': forms.Textarea(attrs={'rows': 1}),
            'explanation': forms.Textarea(attrs={'rows': 1}),
        }

    def __init__(self, *args, **kwargs):
        super(QuestionForm, self).__init__(*args, **kwargs)
        self.helper = EsccaFormHelper()


class QuestionInline(InlineFormSet):
    model = Question
    form_class = QuestionForm
    extra = 1


class AnswerForm(forms.ModelForm):
    class Meta:
        model = Answer
        fields = ('answer', )

    def __init__(self, *args, **kwargs):
        super(AnswerForm, self).__init__(*args, **kwargs)
        self.helper = EsccaFormHelper()

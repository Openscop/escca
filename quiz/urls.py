from __future__ import unicode_literals

from django.conf.urls import url

from .views import GeneralStatsView
from .views import StatsBySubFieldView
from .views import StatsByTopicView
from .views import StatsByQuizView
from .views import StatsByQuestionView

from .views import QuizCreateView
from .views import QuizUpdateView

from .views import QuizListView
from .views import QuizUserListView
from .views import QuizReviewListView
from .views import QuizByTopicListView
from .views import QuizDetailView
from .views import QuizTakeView
from .views import QuizResultView

from .views import QuizAskReviewView
from .views import QuizAddReviewerView
from .views import QuizPopReviewerView
from .views import QuizCommentView
from .views import QuizReviewView
from .views import QuizPublishView
from .views import QuizArchiveView

from .views import MailToUsersView

urlpatterns = [
    url(r'^$', QuizListView.as_view(), name='quiz-list'),
    url(r'^stats/$', GeneralStatsView.as_view(), name='quiz-stats'),
    url(r'^stats/by_subfield/(?P<pk>\d+)$', StatsBySubFieldView.as_view(), name='quiz-stats-by-subfield'),
    url(r'^stats/by_topic/(?P<pk>\d+)$', StatsByTopicView.as_view(), name='quiz-stats-by-topic'),
    url(r'^stats/by_quiz/(?P<pk>\d+)$', StatsByQuizView.as_view(), name='quiz-stats-by-quiz'),
    url(r'^stats/by_question/(?P<pk>\d+)$', StatsByQuestionView.as_view(), name='quiz-stats-by-question'),
    url(r'^review/$', QuizReviewListView.as_view(), name='quiz-review-list'),
    url(r'^user/$', QuizUserListView.as_view(), name='quiz-user-list'),
    url(r'^(?P<fieldslug>[-_\w]+)/(?P<subfieldslug>[-_\w]+)/(?P<topicslug>[-_\w]+)/$',
        QuizByTopicListView.as_view(), name='quiz-by-topic-list'),
    url(r'^create$', QuizCreateView.as_view(), name='quiz-create'),
    url(r'^(?P<pk>\d+)/$', QuizDetailView.as_view(), name='quiz-detail'),
    url(r'^(?P<pk>\d+)/update$', QuizUpdateView.as_view(), name='quiz-update'),
    url(r'^(?P<pk>\d+)/ask_review$', QuizAskReviewView.as_view(), name='quiz-ask-review'),
    url(r'^(?P<pk>\d+)/add-reviewer$', QuizAddReviewerView.as_view(), name='quiz-add-reviewer'),
    url(r'^(?P<pk>\d+)/pop-reviewer$', QuizPopReviewerView.as_view(), name='quiz-pop-reviewer'),
    url(r'^(?P<pk>\d+)/comment$', QuizCommentView.as_view(), name='quiz-comment'),
    url(r'^(?P<pk>\d+)/review$', QuizReviewView.as_view(), name='quiz-review'),
    url(r'^(?P<pk>\d+)/publish$', QuizPublishView.as_view(), name='quiz-publish'),
    url(r'^(?P<pk>\d+)/archive$', QuizArchiveView.as_view(), name='quiz-archive'),
    url(r'^(?P<pk>\d+)/take$', QuizTakeView.as_view(), name='quiz-take'),
    url(r'^(?P<pk>\d+)/result$', QuizResultView.as_view(), name='quiz-result'),
    url(r'^mail_to_users/$', MailToUsersView.as_view(), name='mail_to_users'),
]

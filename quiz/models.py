from __future__ import division
from __future__ import unicode_literals

import datetime

from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.core.mail import send_mass_mail, send_mail, EmailMessage
from django.core.urlresolvers import reverse
from django.db import models
from django.db.models import Sum
from django.db.models.signals import m2m_changed, post_save
from django.dispatch import receiver
from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext, ugettext_lazy as _

from ckeditor.fields import RichTextField

from django_countries.fields import CountryField

from django_fsm import FSMField, transition

from guardian.shortcuts import assign_perm, remove_perm

from model_utils.models import TimeStampedModel

from cytometry.models import Topic

from quiz.emails import QUIZ_PUBLISHED_EMAIL
from quiz.emails import CURRENT_FEES_PAYED_EMAIL
from quiz.emails import NEXT_FEES_PAYED_EMAIL
from quiz.emails import QUIZ_COMMENT_EMAIL
from quiz.emails import USER_CREATED_EMAIL


LEVEL_CHOICES = (
    ('1', _('basic')),
    ('2', _('current')),
    ('3', _('advanced')),
    ('4', _('expert')),
)


def get_current_site():
    return Site.objects.get_current()


@python_2_unicode_compatible
class Year(models.Model):
    year = models.PositiveIntegerField(_('year'))

    class Meta:
        verbose_name = _('year')

    def __str__(self):
        return str(self.year)


@python_2_unicode_compatible
class Profile(models.Model):
    user = models.OneToOneField(User)
    country = CountryField()
    city = models.CharField(_('city'), max_length=255)
    lab_name = models.CharField(_('lab name'), max_length=255, blank=True)
    lab_name_url = models.URLField(_('lab name website'), blank=True)
    accept_emails = models.BooleanField(_('I accept e-mail from ESCCA'), default=True)
    topics_of_interest = models.ManyToManyField(Topic, through='ProfileTopicsOfInterest')
    fees_payed = models.ManyToManyField(Year, blank=True)

    class Meta:
        verbose_name = _('profile')

    def __str__(self):
        return self.user.__str__()

    def get_absolute_url(self):
        return reverse('profile')

    def get_tyscca_id(self):
        return "{}-{}".format(self.user.id, self.user.password[33:39])

    def pay_fees_for_current_year(self):
        current_year = Year.objects.get_or_create(year=datetime.date.today().year)[0]
        self.fees_payed.add(current_year)
        current_site = get_current_site()
        subject = CURRENT_FEES_PAYED_EMAIL['subject'].format(site_name=current_site.name)
        body = CURRENT_FEES_PAYED_EMAIL['body'].format(site_domain=current_site.domain)
        send_mail(
            ugettext(subject),
            ugettext(body),
            settings.DEFAULT_FROM_EMAIL,
            [self.user.email]
        )

    def pay_fees_for_next_year(self):
        next_year = Year.objects.get_or_create(year=datetime.date.today().year + 1)[0]
        self.fees_payed.add(next_year)
        current_site = get_current_site()
        subject = NEXT_FEES_PAYED_EMAIL['subject'].format(site_name=current_site.name)
        body = NEXT_FEES_PAYED_EMAIL['body'].format(site_domain=current_site.domain)
        send_mail(
            ugettext(subject),
            ugettext(body),
            settings.DEFAULT_FROM_EMAIL,
            [self.user.email]
        )

    def fees_payed_for_current_year(self):
        return self.fees_payed.filter(year=datetime.date.today().year).exists()

    def is_possible_reviewer(self, quiz):
        if self.user not in quiz.authors.all():
            if quiz.topic in self.topics_of_interest.filter(profiletopicsofinterest__is_reviewer=True):
                return True
        return False

    def get_reviewer_points(self):
        return sum([quiz.get_max_score() for quiz in self.user.quiz_reviewed.published()])

    def get_author_points(self):
        return sum([quiz.get_max_score() for quiz in self.user.quiz_authored.published()]) * 3

    def get_courses_points(self):
        return sum([lecture.get_teacher_points() for lecture in self.user.lecture_set.all()])

    def get_tutorships_points(self):
        return sum([tutorship.get_tutor_points() for tutorship in self.user.tutorships_followed_set.all()])

    def get_collaboration_points(self):
        return sum([self.get_reviewer_points(), self.get_author_points(),
                    self.get_courses_points(), self.get_tutorships_points()])


def send_user_created_mail(profile):
    current_site = get_current_site()
    subject = USER_CREATED_EMAIL['subject'].format(
        site_name=current_site.name,
        username=profile.user.username,
    )
    body = USER_CREATED_EMAIL['body'].format(
        site_domain=current_site.domain,
        username=profile.user.username,
        first_name=profile.user.first_name,
        last_name=profile.user.last_name,
        email=profile.user.email,
    )
    send_mail(ugettext(subject), ugettext(body),
              settings.DEFAULT_FROM_EMAIL, [settings.EMAIL_HOST_USER])


@receiver(post_save, sender=Profile)
def notify_user_creation(instance, created, **kwargs):
    if created:
        send_user_created_mail(instance)


class ProfileTopicsOfInterestQuerySet(models.QuerySet):
    def experts(self):
        return self.filter(is_reviewer=True)


@python_2_unicode_compatible
class ProfileTopicsOfInterest(models.Model):
    profile = models.ForeignKey(Profile)
    topic = models.ForeignKey(Topic)
    is_reviewer = models.BooleanField(_('I would like to be part of reviewers team for this topic'),
                                      default=False)
    objects = ProfileTopicsOfInterestQuerySet.as_manager()

    def __str__(self):
        return "{} {}".format(self.profile.user.username, self.topic)


class State(object):
    DRAFT = 'draft'
    PENDING_REVIEWER = 'pending_reviewer'
    PENDING_REVIEW = 'pending_review'
    REVIEWED = 'reviewed'
    PUBLISHED = 'published'
    ARCHIVED = 'archived'

    CHOICES = (
        (DRAFT, _('draft')),
        (PENDING_REVIEWER, _('pending reviewer')),
        (PENDING_REVIEW, _('pending review')),
        (REVIEWED, _('reviewed')),
        (PUBLISHED, _('published')),
        (ARCHIVED, _('archived')),
    )


class QuizQuerySet(models.QuerySet):
    def published(self):
        return self.filter(state=State.PUBLISHED)


@python_2_unicode_compatible
class Quiz(TimeStampedModel):
    title = models.CharField(_('titre'), max_length=255, unique=True)
    topic = models.ForeignKey(Topic, verbose_name=_('topic'))
    level = models.CharField(_('level'), max_length=1, choices=LEVEL_CHOICES)
    authors = models.ManyToManyField(User,
                                     related_name='quiz_authored')
    reviewers = models.ManyToManyField(User,
                                       related_name='quiz_reviewed')
    description = RichTextField(_('description'))
    explanation = RichTextField(
        _('explanation'),
        help_text=_('Explanation will be displayed once the user has taken the quiz.'),
    )
    get_home_message = models.TextField(_('get home message'), blank=True)
    state = FSMField(
        verbose_name=_('publication state'),
        default=State.DRAFT,
        choices=State.CHOICES,
    )
    pub_date = models.DateTimeField(_('publication date'), blank=True, null=True)
    objects = QuizQuerySet.as_manager()

    class Meta:
        ordering = ['-pub_date', 'title']
        verbose_name = _('quiz')
        verbose_name_plural = _('quizzes')
        permissions = (
            ('review_quiz', _('Review quiz')),
        )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('quiz:quiz-detail', kwargs={'pk': self.pk})

    def notify_publication(self):
        current_site = get_current_site()
        subject = QUIZ_PUBLISHED_EMAIL['subject'].format(
            site_name=current_site.name,
            quiz_title=self.title,
        )
        body = QUIZ_PUBLISHED_EMAIL['body'].format(
            site_domain=current_site.domain,
            ref=self.get_absolute_url(),
        )
        email_tuple = ((
            ugettext(subject),
            ugettext(body),
            settings.DEFAULT_FROM_EMAIL,
            [profile.user.email]
        ) for profile in self.topic.profile_set.filter(accept_emails=True))
        send_mass_mail(email_tuple)

    def has_questions(self):
        return self.question_set.all().exists()
    has_questions.hint = _('a quiz must have at least one question')

    def is_new(self):
        return (self.created + datetime.timedelta(weeks=12)) > timezone.now()

    def send_comment_to_author(self, message, reviewer):
        current_site = get_current_site()
        subject = QUIZ_COMMENT_EMAIL['subject'].format(
            site_name=current_site.name,
            quiz_title=self
        )
        body = message + QUIZ_COMMENT_EMAIL['body'].format(
            site_domain=current_site.domain,
        )
        email = EmailMessage(
            subject=subject,
            body=body,
            to=[author.email for author in self.authors.all()],
            headers={'Reply-To': reviewer}
        )
        email.send()

    def weight(self):
        '''
        The weight indicates the sum of all question's ponderation
        '''
        return self.question_set.aggregate(weight=Sum('ponderation'))['weight']

    def get_max_score(self):
        return sum([question.get_max_score() for question in self.question_set.all()])

    ############################################################
    # State Transitions

    @transition(field=state,
                source=[State.DRAFT],
                target=State.PENDING_REVIEWER,
                conditions=[has_questions])
    def ask_reviewer(self):
        pass

    @transition(field=state,
                source=[State.PENDING_REVIEWER],
                target=State.PENDING_REVIEW,
                conditions=[])
    def add_reviewer(self, user):
        self.reviewers.add(user)

    @transition(field=state,
                source=[State.PENDING_REVIEW],
                target=State.PENDING_REVIEWER,
                conditions=[])
    def pop_reviewer(self, user):
        self.reviewers.remove(user)

    @transition(field=state,
                source=[State.PENDING_REVIEW],
                target=State.REVIEWED,
                conditions=[])
    def review(self):
        pass

    @transition(field=state,
                source=[State.REVIEWED, State.ARCHIVED],
                target=State.PUBLISHED,
                conditions=[])
    def publish(self):
        if not self.state == State.ARCHIVED:
            self.pub_date = timezone.now()
            self.notify_publication()

    @transition(field=state,
                source=State.PUBLISHED,
                target=State.ARCHIVED,
                conditions=[])
    def archive(self):
        pass


@receiver(m2m_changed, sender=Quiz.authors.through)
def update_quiz_change_perms(action, instance, pk_set, **kwargs):
    if action == "post_add":
        for pk in pk_set:
            user = User.objects.get(pk=pk)
            if not user.has_perm('quiz.change_quiz', instance):
                assign_perm('quiz.change_quiz', user, instance)
    elif action == "post_remove":
        for pk in pk_set:
            user = User.objects.get(pk=pk)
            if user.has_perm('quiz.change_quiz', instance):
                remove_perm('quiz.change_quiz', user, instance)


@receiver(m2m_changed, sender=Quiz.reviewers.through)
def update_quiz_review_perms(action, instance, pk_set, **kwargs):
    if action == "post_add":
        for pk in pk_set:
            user = User.objects.get(pk=pk)
            if not user.has_perm('quiz.review_quiz', instance):
                assign_perm('quiz.review_quiz', user, instance)
    elif action == "post_remove":
        for pk in pk_set:
            user = User.objects.get(pk=pk)
            if user.has_perm('quiz.review_quiz', instance):
                remove_perm('quiz.review_quiz', user, instance)


@python_2_unicode_compatible
class Question(TimeStampedModel):
    PONDERATION_CHOICES = (
        (1, 1),
        (2, 2),
        (3, 3),
        (4, 4),
        (5, 5),
    )
    quiz = models.ForeignKey(Quiz, verbose_name=_('quiz'))
    question = models.TextField(_('question'))
    ponderation = models.IntegerField(_('ponderation'), choices=PONDERATION_CHOICES, default=1)
    explanation = models.TextField(_('explanation'))
    correct = models.BooleanField(
        default=False,
        help_text=_('Tick this if the question is true. Leave it blank for false.')
    )

    class Meta:
        verbose_name = _('question')

    def __str__(self):
        return self.question

    def get_max_score(self):
        return round(self.ponderation * self.quiz.question_set.all().count() / self.quiz.weight(), 2)


class SittingQuerySet(models.QuerySet):
    def overall_right(self):
        return sum(sitting.right_answers() for sitting in self.all())

    def overall_wrong(self):
        return sum(sitting.wrong_answers() for sitting in self.all())

    def overall_unknown(self):
        return sum(sitting.unknown_answers() for sitting in self.all())

    def overall_questions(self):
        return sum(sitting.answer_set.all().count() for sitting in self.all())

    def overall_right_percentage(self):
        return round(self.overall_right() * 100 / self.overall_questions(), 1)

    def overall_wrong_percentage(self):
        return round(self.overall_wrong() * 100 / self.overall_questions(), 1)

    def overall_unknown_percentage(self):
        return 100 - self.overall_wrong_percentage() - self.overall_right_percentage()

    def overall_score(self):
        return sum(sitting.score() for sitting in self)

    def only_best_scores(self):
        """
        Excludes all initial attempts related to a lecture and return
        only sittings with the best score for the same quiz
        """
        queryset = self.exclude(lecturesitting__first_attempt=True)
        scores = {}
        excluded = []
        for sitting in queryset:
            if sitting.quiz.id in scores.keys():
                if sitting.score() > scores[sitting.quiz.id][0]:
                    excluded.append(scores[sitting.quiz.id][1])
                    scores[sitting.quiz.id] = (sitting.score(), sitting.id)
                else:
                    excluded.append(sitting.id)
            else:
                scores[sitting.quiz.id] = (sitting.score(), sitting.id)
        return queryset.exclude(id__in=excluded)


@python_2_unicode_compatible
class Sitting(TimeStampedModel):
    quiz = models.ForeignKey(Quiz, verbose_name=_('quiz'))
    user = models.ForeignKey(User, verbose_name=_('user'))
    objects = SittingQuerySet.as_manager()

    class Meta:
        verbose_name = _('sitting')

    def __str__(self):
        return '{user} - {quiz}'.format(user=self.user.__str__().decode('utf8'),
                                        quiz=self.quiz.__str__().decode('utf8'))

    def right_answers(self):
        return sum(1 for answer in self.answer_set.all() if answer.is_correct())

    def wrong_answers(self):
        return sum(1 for answer in self.answer_set.all() if answer.is_wrong())

    def unknown_answers(self):
        return self.answer_set.filter(answer=None).count()

    def questions(self):
        return self.answer_set.all().count()

    def right_answers_percentage(self):
        return self.right_answers() * 100 / self.questions()

    def wrong_answers_percentage(self):
        return self.wrong_answers() * 100 / self.questions()

    def unknown_answers_percentage(self):
        return self.unknown_answers() * 100 / self.questions()

    def score(self):
        return sum([answer.score() for answer in self.answer_set.all()])


class AnswerQuerySet(models.QuerySet):
    def rights_percentage(self, question):
        return round(self.filter(answer=question.correct).count() * 100 / self.all().count(), 1)

    def wrongs_percentage(self, question):
        return 100 - self.rights_percentage(question) - self.unknown_percentage(question)

    def unknown_percentage(self, question):
        return round(self.filter(answer=None).count() * 100 / self.all().count(), 1)


@python_2_unicode_compatible
class Answer(TimeStampedModel):
    question = models.ForeignKey(Question, verbose_name=_('question'))
    sitting = models.ForeignKey(Sitting, verbose_name=_('sitting'))
    answer = models.NullBooleanField(_('answer'))
    objects = AnswerQuerySet.as_manager()

    class Meta:
        verbose_name = _('answer')

    def __str__(self):
        return '{} - {} - {}'.format(self.sitting.user, self.question, self.answer)

    def is_correct(self):
        return self.answer == self.question.correct

    def is_wrong(self):
        if self.answer is None:
            return False
        else:
            return not self.is_correct()

    def score(self):
        points = self.question.get_max_score()
        if self.is_correct():
            score = points
        elif self.answer is None:
            score = 0
        else:
            score = -points
        return score

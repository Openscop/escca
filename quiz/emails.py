from __future__ import unicode_literals


USER_CREATED_EMAIL = {
    'subject': "[{site_name} Administration] New Account Created : {username}",
    'body': (
        "A new account has been created : \n"
        "Username : {username}\n"
        "First Name : {first_name}\n"
        "Last Name : {last_name}\n\n"
        "Please, verify if the related person has payed his fees and follow this link in order "
        "to grant him ESCCA rights to the application :\n"
        "http://{site_domain}/admin/auth/user/?q={email}\n"
        "(Select the desired user, select 'mark fees of selected users payed for the year'"
        " in the action box and click 'Go'\n"
        "\n\nThanks\n"
        "--\n"
        "The {site_domain} Team\n"
    ),
}


QUIZ_PUBLISHED_EMAIL = {
    'subject': "[{site_name}] New quiz available : {quiz_title}",
    'body': (
        "A new quiz has been published in one of the topics you're interested in.\n"
        "To pass the quiz, please, follow this link http://{site_domain}{ref}"
        "\n\nThanks\n"
        "--\n"
        "The {site_domain} Team\n"
    ),
}


CURRENT_FEES_PAYED_EMAIL = {
    'subject': "[{site_name}] Your subscription fees have been validated",
    'body': (
        "You now have full access to the TySCCA website"
        "\n\nThanks\n"
        "--\n"
        "The {site_domain} Team\n"
    ),
}


NEXT_FEES_PAYED_EMAIL = {
    'subject': "[{site_name}] Your subscription fees have been validated for next year",
    'body': (
        "Your rights on the platform will be kept for next year"
        "\n\nThanks\n"
        "--\n"
        "The {site_domain} Team\n"
    ),
}


QUIZ_COMMENT_EMAIL = {
    'subject': '[{site_name}] You received a comment for your quiz : {quiz_title}',
    'body': (
        "\n\n*********"
        "\nPS : The person who sent you this comment won't know your email address until"
        "the moment you respond this email."
        "\n\nThanks\n"
        "--\n"
        "The {site_domain} Team\n"
    ),
}

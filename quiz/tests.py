from django.test import TestCase

from .admin import QuestionInline

from .factories import UserFactory
from .factories import QuizFactory
from .factories import QuestionFactory

from .forms import EsccaFormHelper
from .forms import QuizForm
from .forms import QuestionForm

from .models import Quiz
from .models import Question

from .views import QuizCreateView
from .views import QuizUpdateView


class QuizAdminTest(TestCase):

    def test_question_inline(self):
        admin = QuestionInline(None, None)
        self.assertEqual(admin.model, Question)
        self.assertEqual(admin.extra, 1)


class EsccaFormHelperTest(TestCase):

    def test_init(self):
        helper = EsccaFormHelper()
        self.assertEqual(len(helper.inputs), 0)
        self.assertEqual(helper.form_class, 'form-horizontal')
        self.assertEqual(helper.label_class, 'col-sm-2')
        self.assertEqual(helper.field_class, 'col-sm-10')


class QuizFormTest(TestCase):

    def test_init(self):
        form = QuizForm()
        self.assertEqual(form._meta.model, Quiz)


class QuestionFormTest(TestCase):

    def test_init(self):
        form = QuestionForm()
        self.assertEqual(form._meta.model, Question)
        self.assertEqual(form._meta.widgets['explanation'].attrs['rows'], 1)
        self.assertEqual(form._meta.widgets['question'].attrs['rows'], 1)


class QuizUpdateViewTest(TestCase):

    def test_init(self):
        view = QuizUpdateView()
        self.assertEqual(view.get_form_class(), QuizForm)


class QuizCreateViewTest(TestCase):

    def test_init(self):
        view = QuizCreateView()
        self.assertEqual(view.get_form_class(), QuizForm)


class QuizTest(TestCase):

    def test_str(self):
        quiz = QuizFactory()
        self.assertEqual(quiz.__str__(), quiz.title)

    def test_change_perms(self):
        quiz = QuizFactory()
        user = UserFactory()
        self.assertFalse(user.has_perm('quiz.change_quiz', quiz))
        quiz.authors.add(user)
        self.assertTrue(user.has_perm('quiz.change_quiz', quiz))
        quiz.authors.remove(user)
        self.assertFalse(user.has_perm('quiz.change_quiz', quiz))


class QuestionTest(TestCase):

    def test_str(self):
        question = QuestionFactory()
        self.assertEqual(question.__str__(), question.question)

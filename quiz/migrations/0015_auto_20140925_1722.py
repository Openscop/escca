# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


# pylint: disable=C0301
class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0014_auto_20140925_1249'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='quiz',
            options={'ordering': ['-pub_date', 'title'], 'verbose_name': 'quiz', 'verbose_name_plural': 'quizzes'},
        ),
        migrations.AddField(
            model_name='quiz',
            name='pub_date',
            field=models.DateTimeField(null=True, verbose_name='publication date', blank=True),
            preserve_default=True,
        ),
    ]

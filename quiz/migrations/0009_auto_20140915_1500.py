# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


# pylint: disable=C0301
class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0008_auto_20140911_1744'),
    ]

    operations = [
        migrations.CreateModel(
            name='Year',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('year', models.PositiveIntegerField(verbose_name='year')),
            ],
            options={
                'verbose_name': 'year',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='profile',
            name='fees_payed',
            field=models.ManyToManyField(blank=True, to='quiz.Year', null=True),
            preserve_default=True,
        ),
    ]

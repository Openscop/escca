# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0030_sitting_post_quiz'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='sitting',
            name='post_quiz',
        ),
        migrations.RemoveField(
            model_name='sitting',
            name='pre_quiz',
        ),
    ]

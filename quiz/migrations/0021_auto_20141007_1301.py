# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations  # pylint: disable=W0611


# pylint: disable=C0301
def update_quiz_expired_fields(apps, schema_editor):
    Quiz = apps.get_model('quiz', 'Quiz')
    for quiz in Quiz.objects.all():
        if quiz.state == 'E':
            quiz.state = 'A'
            quiz.save()


class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0020_auto_20141007_1301'),
    ]

    operations = [
        migrations.RunPython(update_quiz_expired_fields),
    ]

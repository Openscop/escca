# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


# pylint: disable=C0301
class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('quiz', '0017_auto_20140929_1006'),
    ]

    operations = [
        migrations.AddField(
            model_name='quiz',
            name='reviewers',
            field=models.ManyToManyField(related_name='quiz_reviewed', to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='quiz',
            name='authors',
            field=models.ManyToManyField(related_name='quiz_authored', to=settings.AUTH_USER_MODEL),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations  # pylint: disable=W0611


# pylint: disable=C0301
def update_quiz_state_fields(apps, schema_editor):
    Quiz = apps.get_model('quiz', 'Quiz')
    for quiz in Quiz.objects.all():
        if quiz.state == 'draft':
            quiz.state = 'D'
            quiz.save()
        elif quiz.state == 'published':
            quiz.state = 'P'
            quiz.save()
        elif quiz.state == 'expired':
            quiz.state = 'E'
            quiz.save()


class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0016_auto_20140929_1006'),
    ]

    operations = [
        migrations.RunPython(update_quiz_state_fields),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


# pylint: disable=C0301
class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0003_profile_accept_emails'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='lab_name_url',
            field=models.URLField(verbose_name='lab name url', blank=True, default='http://www.google.com'),
            preserve_default=False,
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0031_auto_20150330_1341'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='fees_payed',
            field=models.ManyToManyField(to='quiz.Year', blank=True),
        ),
    ]

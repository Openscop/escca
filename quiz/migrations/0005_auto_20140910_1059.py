# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


# pylint: disable=C0301
class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0004_profile_lab_name_url'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='lab_name_url',
            field=models.URLField(blank=True, verbose_name='lab name website'),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations  # pylint: disable=W0611
import django_fsm


# pylint: disable=C0301
class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0026_auto_20141014_1623'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='quiz',
            options={'ordering': ['-pub_date', 'title'], 'verbose_name': 'quiz', 'verbose_name_plural': 'quizzes', 'permissions': (('review_quiz', 'Review quiz'),)},  # noqa
        ),
        migrations.AlterField(
            model_name='quiz',
            name='state',
            field=django_fsm.FSMField(default='draft', max_length=50, verbose_name='publication state', choices=[('draft', 'draft'), ('pending_reviewer', 'pending reviewer'), ('pending_review', 'pending review'), ('reviewed', 'reviewed'), ('published', 'published'), ('archived', 'archived')]),  # noqa
        ),
    ]

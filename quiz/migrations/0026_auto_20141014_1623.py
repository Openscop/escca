# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations  # pylint: disable=W0611


# pylint: disable=C0301
def update_quiz_state_fields(apps, schema_editor):
    Quiz = apps.get_model('quiz', 'Quiz')
    for quiz in Quiz.objects.all():
        if quiz.state == 'D':
            quiz.state = 'draft'
            quiz.save()
        elif quiz.state == 'P':
            quiz.state = 'published'
            quiz.save()
        elif quiz.state == 'A':
            quiz.state = 'archived'
            quiz.save()
        elif quiz.state == 'N':
            quiz.state = 'pending_review'
            quiz.save()
        elif quiz.state == 'R':
            quiz.state = 'reviewed'
            quiz.save()


class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0025_profiletopicsofinterest_is_reviewer'),
    ]

    operations = [
        migrations.RunPython(update_quiz_state_fields),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import ckeditor.fields


# pylint: disable=C0301
class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0007_auto_20140910_1532'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='answerer',
            field=models.BooleanField(verbose_name='I accept answering questions from other members of the platform', default=False),  # noqa
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='profile',
            name='tutor',
            field=models.BooleanField(verbose_name='I accept beeing a tutor', default=False),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='question',
            name='correct',
            field=models.BooleanField(help_text='Tick this if the question is true. Leave it blank for false.', default=False),  # noqa
        ),
        migrations.AlterField(
            model_name='quiz',
            name='explanation',
            field=ckeditor.fields.RichTextField(verbose_name='explanation', help_text='Explanation will be displayed once the user has taken the quiz.'),  # noqa
        ),
    ]

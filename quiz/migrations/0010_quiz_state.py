# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations  # pylint: disable=W0611
import django_fsm


# pylint: disable=C0301
class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0009_auto_20140915_1500'),
    ]

    operations = [
        migrations.AddField(
            model_name='quiz',
            name='state',
            field=django_fsm.FSMField(default='draft', protected=True, max_length=50, verbose_name='publication state', choices=[('draft', 'draft'), ('published', 'published'), ('expired', 'expired')]),  # noqa
            preserve_default=True,
        ),
    ]

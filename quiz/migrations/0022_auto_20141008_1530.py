# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations  # pylint: disable=W0611


# pylint: disable=C0301
class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0021_auto_20141007_1301'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='profile',
            name='answerer',
        ),
        migrations.RemoveField(
            model_name='profile',
            name='tutor',
        ),
    ]

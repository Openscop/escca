# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


# pylint: disable=C0301
class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0013_auto_20140918_1611'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='answerer',
            field=models.BooleanField(default=False, help_text='\n                  You may be asked to answer a punctual question in your field.\n                  Question will be sent to you and you will have to answer by mail to the requester.\n\n                  For highly relevant question, you are encourage to submit a short report,\n                  letter and a new Quizz of interest, to be available for more cytometrists', verbose_name='I accept answering questions from other members of the platform'),  # noqa
        ),
        migrations.AlterField(
            model_name='profile',
            name='tutor',
            field=models.BooleanField(default=False, help_text='\n                  You may be asked to supervise a project on its progress, reviewing the project,\n                  the concept, the methodology, the results, interpretation, publication on request,\n                  beside the local formal tutor of the project.\n                  As rewarding, you should be a co-author of the production.\n                  A contract between the two persons, including the formal tutor is proposed.\n                  ', verbose_name='I accept beeing a tutor'),  # noqa
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


# pylint: disable=C0301
class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0002_auto_20140905_1039'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='accept_emails',
            field=models.BooleanField(verbose_name='I accept e-mail from ESCCA', default=True),
            preserve_default=True,
        ),
    ]

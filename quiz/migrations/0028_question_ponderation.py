# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


# pylint: disable=C0301
class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0027_auto_20141015_1004'),
    ]

    operations = [
        migrations.AddField(
            model_name='question',
            name='ponderation',
            field=models.IntegerField(default=1, verbose_name='ponderation', choices=[(1, 1), (2, 2), (3, 3), (4, 4), (5, 5)]),  # noqa
            preserve_default=True,
        ),
    ]

from __future__ import unicode_literals

import datetime

from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _

from ckeditor.fields import RichTextField

from guardian.shortcuts import assign_perm
from guardian.shortcuts import remove_perm

from cytometry.models import Topic

from .emails import send_tutorship_accepted
from .emails import confirm_tutorship_accepted
from .emails import send_tutorship_canceled


@python_2_unicode_compatible
class Tutorship(models.Model):
    title = models.CharField(_('title'), max_length=255)
    topic = models.ForeignKey(Topic, verbose_name=_('topic'))
    start = models.DateField(_('start date of the tutoring'), blank=True, null=True)
    description = RichTextField(_('description'))
    tutoree = models.ForeignKey(User, verbose_name=_('tutoree'), blank=True, null=True)
    tutor = models.ForeignKey(
        User,
        verbose_name=_('tutor'),
        blank=True,
        null=True,
        related_name='tutorships_followed_set',
    )

    class Meta:
        verbose_name = _('tutoring')

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('tutorships:tutorship-detail', kwargs={'pk': self.pk})

    def remove_tutor(self):
        send_tutorship_canceled(self.tutoree, self.tutor, self)
        for step in self.step_set.all():
            try:
                step.evaluation.delete()
            except:
                pass
            else:
                step.evaluation_request_sent = False
                step.save()
        remove_perm('change_tutorship', self.tutor, self)

    def inform_tutorship_accepted(self):
        send_tutorship_accepted(self.tutoree, self.tutor, self)
        confirm_tutorship_accepted(self.tutoree, self.tutor, self)

    def has_late_evaluation(self):
        if self.step_set.deadline_passed().exists():
            try:
                self.step_set.last().evaluation
            except:
                if (self.step_set.last().get_deadline().date() + datetime.timedelta(weeks=4)) < datetime.date.today():
                    return True
        return False

    def get_tutor_points(self):
        return sum([step.get_tutor_points() for step in self.step_set.all()])


@receiver(post_save, sender=Tutorship)
def assign_tutorship_perms(instance, created, **kwargs):
    if created:
        assign_perm('change_tutorship', instance.tutoree, instance)
        assign_perm('delete_tutorship', instance.tutoree, instance)
    if instance.tutor:
        assign_perm('change_tutorship', instance.tutor, instance)


class StepQuerySet(models.QuerySet):
    def deadline_passed(self):
        today = datetime.date.today()
        queryset = self.exclude(tutorship__start=None).filter(evaluation_request_sent=False)
        excluded = []
        for step in queryset:
            if step.get_deadline().date() > today:
                excluded.append(step.id)
        return queryset.exclude(id__in=excluded)


@python_2_unicode_compatible
class Step(models.Model):
    title = models.CharField(_('title'), max_length=255)
    description = RichTextField(_('description'))
    duration = models.PositiveIntegerField(_('duration'))
    tutorship = models.ForeignKey(Tutorship, verbose_name=_('tutoring'))
    evaluation_request_sent = models.BooleanField(default=False)
    objects = StepQuerySet.as_manager()

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('tutorships:tutorship-detail', kwargs={'pk': self.tutorship.pk})

    def get_deadline(self):
        """
        Adds the start of the tutoring and all the durations of steps before this one,
        including this one
        Return it in a datetime object
        """
        return datetime.datetime.combine(
            self.tutorship.start + datetime.timedelta(
                weeks=sum([step.duration for step in self.tutorship.step_set.filter(pk__lte=self.pk)])
            ),
            datetime.datetime.min.time()
        )

    def get_tutor_points(self):
        if datetime.date.today() > self.get_deadline().date() and hasattr(self, 'evaluation'):
            return self.evaluation.satisfaction * self.duration / 100
        else:
            return 0


def validate_percentage(value):
    if not 0 <= value <= 100:
        raise ValidationError("satisfaction must be a percentage")


@python_2_unicode_compatible
class Evaluation(models.Model):
    satisfaction = models.PositiveIntegerField(_('satisfaction'), validators=[validate_percentage])
    comment = models.TextField(_('comment'))
    step = models.OneToOneField(Step, verbose_name=_('step'))

    def __str__(self):
        return "Evaluation - {}".format(self.step)

    def get_absolute_url(self):
        return reverse('tutorships:tutorship-detail', kwargs={'pk': self.step.tutorship.pk})

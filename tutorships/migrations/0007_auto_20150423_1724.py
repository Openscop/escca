# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tutorships', '0006_auto_20150423_1720'),
    ]

    operations = [
        migrations.AlterField(
            model_name='evaluation',
            name='step',
            field=models.OneToOneField(verbose_name='evaluation', to='tutorships.Step'),
            preserve_default=True,
        ),
    ]

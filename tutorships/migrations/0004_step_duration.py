# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tutorships', '0003_auto_20150421_1726'),
    ]

    operations = [
        migrations.AddField(
            model_name='step',
            name='duration',
            field=models.PositiveIntegerField(default=2, verbose_name='duration'),
            preserve_default=False,
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tutorships', '0007_auto_20150423_1724'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='tutorship',
            options={'verbose_name': 'tutoring'},
        ),
        migrations.AlterField(
            model_name='evaluation',
            name='step',
            field=models.OneToOneField(verbose_name='step', to='tutorships.Step'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='step',
            name='tutorship',
            field=models.ForeignKey(verbose_name='tutoring', to='tutorships.Tutorship'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='tutorship',
            name='start',
            field=models.DateField(null=True, verbose_name='start date of the tutoring', blank=True),
            preserve_default=True,
        ),
    ]

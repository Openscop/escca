# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tutorships', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tutorship',
            name='start',
            field=models.DateField(verbose_name='start date of the tutorship'),
            preserve_default=True,
        ),
    ]

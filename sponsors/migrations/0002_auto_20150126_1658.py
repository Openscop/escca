# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('sponsors', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='sponsor',
            name='end_display',
            field=models.DateField(default=datetime.date(2015, 1, 26)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='sponsor',
            name='start_display',
            field=models.DateField(default=datetime.date(2015, 1, 26)),
            preserve_default=False,
        ),
    ]

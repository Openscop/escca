# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Sponsor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=255, verbose_name='name')),
                ('url', models.URLField(verbose_name='website')),
                ('logo', models.ImageField(upload_to='logos/%Y/%m/%d', verbose_name='logo')),
                ('description', models.TextField(verbose_name='description')),
            ],
            options={
                'verbose_name': 'sponsor',
            },
            bases=(models.Model,),
        ),
    ]

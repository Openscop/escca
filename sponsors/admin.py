from __future__ import unicode_literals

from django.contrib import admin

from .models import Sponsor


admin.site.register(Sponsor)

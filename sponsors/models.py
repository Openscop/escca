from __future__ import unicode_literals

import datetime

from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _


class SponsorQuerySet(models.QuerySet):
    def displayable(self):
        return self.filter(start_display__lte=datetime.date.today(),
                           end_display__gte=datetime.date.today())


@python_2_unicode_compatible
class Sponsor(models.Model):
    name = models.CharField(_('name'), max_length=255, unique=True)
    url = models.URLField(_('website'))
    logo = models.ImageField(_('logo'), upload_to='logos/%Y/%m/%d')
    description = models.TextField(_('description'))
    start_display = models.DateField()
    end_display = models.DateField()
    objects = SponsorQuerySet.as_manager()

    class Meta:
        verbose_name = _('sponsor')

    def __str__(self):
        return self.name

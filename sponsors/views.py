from __future__ import unicode_literals

from django.views.generic import DetailView, ListView

from .models import Sponsor


class SponsorDetailView(DetailView):
    model = Sponsor


class SponsorListView(ListView):
    model = Sponsor

    def get_queryset(self):
        return Sponsor.objects.displayable()

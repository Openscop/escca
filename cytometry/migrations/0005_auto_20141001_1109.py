# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations  # pylint: disable=W0611
import django_extensions.db.fields


# pylint: disable=C0301
class Migration(migrations.Migration):

    dependencies = [
        ('cytometry', '0004_auto_20141001_1059'),
    ]

    operations = [
        migrations.AlterField(
            model_name='field',
            name='slug',
            field=django_extensions.db.fields.AutoSlugField(editable=False, populate_from='name', blank=True, verbose_name='slug', overwrite=True),  # noqa
        ),
        migrations.AlterField(
            model_name='subfield',
            name='slug',
            field=django_extensions.db.fields.AutoSlugField(editable=False, populate_from='name', blank=True, verbose_name='slug', overwrite=True),  # noqa
        ),
        migrations.AlterField(
            model_name='topic',
            name='slug',
            field=django_extensions.db.fields.AutoSlugField(editable=False, populate_from='name', blank=True, verbose_name='slug', overwrite=True),  # noqa
        ),
    ]

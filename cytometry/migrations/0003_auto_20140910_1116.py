# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


# pylint: disable=C0301
class Migration(migrations.Migration):

    dependencies = [
        ('cytometry', '0002_subfield'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='subfield',
            options={'verbose_name': 'sub-field'},
        ),
        migrations.RemoveField(
            model_name='topic',
            name='field',
        ),
        migrations.AddField(
            model_name='topic',
            name='subfield',
            field=models.ForeignKey(to='cytometry.SubField', verbose_name='sub-field', default=1),
            preserve_default=False,
        ),
    ]

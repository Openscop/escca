from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _

from django_extensions.db.fields import AutoSlugField


@python_2_unicode_compatible
class Field(models.Model):
    name = models.CharField(_('name'), max_length=255, unique=True)
    slug = AutoSlugField(_('slug'), populate_from='name', overwrite=True)

    class Meta:
        verbose_name = _('field')
        ordering = ['name']

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class SubField(models.Model):
    name = models.CharField(_('name'), max_length=255, unique=True)
    slug = AutoSlugField(_('slug'), populate_from='name', overwrite=True)
    field = models.ForeignKey(Field, verbose_name=_('field'))

    class Meta:
        verbose_name = _('sub-field')
        ordering = ['name']

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class Topic(models.Model):
    name = models.CharField(_('name'), max_length=255, unique=True)
    slug = AutoSlugField(_('slug'), populate_from='name', overwrite=True)
    subfield = models.ForeignKey(SubField, verbose_name=_('sub-field'))

    class Meta:
        verbose_name = _('topic')
        ordering = ['name']

    def __str__(self):
        return self.name

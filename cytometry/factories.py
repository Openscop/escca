from __future__ import unicode_literals

import factory

from .models import Field
from .models import SubField
from .models import Topic


class FieldFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = Field

    name = factory.Sequence(lambda n: 'Field_{0}'.format(n))


class SubFieldFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = SubField

    name = factory.Sequence(lambda n: 'SubField_{0}'.format(n))
    field = factory.SubFactory(FieldFactory)


class TopicFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = Topic

    name = factory.Sequence(lambda n: 'Topic_{0}'.format(n))
    subfield = factory.SubFactory(SubFieldFactory)

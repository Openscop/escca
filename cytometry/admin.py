from django.contrib import admin

from .models import Field
from .models import SubField
from .models import Topic


class SubFieldInline(admin.TabularInline):
    model = SubField
    extra = 1


class TopicInline(admin.TabularInline):
    model = Topic
    extra = 1


class FieldAdmin(admin.ModelAdmin):
    inlines = [
        SubFieldInline,
    ]
    search_fields = ('name', )


class SubFieldAdmin(admin.ModelAdmin):
    inlines = [
        TopicInline,
    ]
    list_display = ('name', 'field', )
    list_filter = ('field', )
    search_fields = ('name', )


class TopicAdmin(admin.ModelAdmin):
    list_display = ('name', 'subfield', )
    list_editable = ('subfield', )
    list_filter = ('subfield', 'subfield__field', )
    search_fields = ('name', )


admin.site.register(Field, FieldAdmin)
admin.site.register(SubField, SubFieldAdmin)
admin.site.register(Topic, TopicAdmin)
